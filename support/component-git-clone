#!/usr/bin/env ruby
# frozen_string_literal: true

# Example:
#
#   GIT_REVISION=<revision> ./support/component-git-clone <depth_param> <repo_name> <component_name>
#

require_relative '../lib/gdk'

PROGNAME = File.basename(__FILE__)

def clone(git_clone_args)
  default_args = ['--quiet']
  command = %w[git clone] + default_args + git_clone_args

  3.times do
    sh = Shellout.new(command)
    sh.stream
    return true if sh.success?
  end

  false
end

def checkout(revision)
  %W[git checkout #{revision}]
end

def main(argv)
  revision = ENV['GIT_REVISION']
  clone(argv)
  checkout(revision) if revision
end

abort "Usage: #{PROGNAME} [options]" if ARGV.count < 1

main(ARGV)
